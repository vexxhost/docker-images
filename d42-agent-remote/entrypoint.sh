#!/bin/bash

set -xe

BUILD_MODIFY_TIME=$(stat -c %y /usr/local/bin/d42_autodisc_linux_x64)
/usr/local/bin/d42_autodisc_linux_x64

# NOTE(mnaser): If Device42 self-updates, it restarts itself but the problem
#               with this is that in a containerized environment, we will be
#               in an update loop.
NEW_MODIFY_TIME=$(stat -c %y /usr/local/bin/d42_autodisc_linux_x64)
if [ "$BUILD_MODIFY_TIME" != "$NEW_MODIFY_TIME" ]; then
	/usr/local/bin/d42_autodisc_linux_x64
fi

